// problem1.js
// Function to extract email addresses
function getEmailAddresses(arrayOfObjects) {
    const emailAddresses = [];
    
    // Iterate over each object in arrayOfObjects
    for (let i = 0; i < arrayOfObjects.length; i++) {
      emailAddresses.push(arrayOfObjects[i].email);
    }
    
    return emailAddresses;
  }
  
  module.exports = getEmailAddresses;
  