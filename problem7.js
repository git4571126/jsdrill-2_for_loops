
function printNamesAndEmails(arrayOfObjects) {
    // Check if arrayOfObjects is an array
    if (typeof arrayOfObjects !== 'object' || !Array.isArray(arrayOfObjects)) {
        throw new Error('Invalid input. Expected an array of objects.');
    }    
    // Iterate over each object
    for (let i = 0; i < arrayOfObjects.length; i++) {
        const person = arrayOfObjects[i];
        // Check if the person's age is 25
        if (person.age === 25) {
            const name = person.name || 'unknown';
            const email = person.email || 'No email';
            console.log(`Name: ${name}, Email: ${email}`);
        }
    }
}

module.exports = printNamesAndEmails;
