function logCityAndCountry(arrayOfObjects) {
    // Check if arrayOfObjects is an array
    if (!Array.isArray(arrayOfObjects)) {
        throw new Error('Invalid input. Expected an array of objects.');
    }

    // Iterate over each object in arrayOfObjects and log city and country
    for (let i = 0; i < arrayOfObjects.length; i++) {
        const person = arrayOfObjects[i];
        const city = person.city || 'unknown';
        const country = person.country || 'unknown';
        console.log(`City: ${city}, Country: ${country}`);
    }
}

module.exports = logCityAndCountry;
