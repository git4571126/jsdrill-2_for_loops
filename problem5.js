// problem5.js

function printAges(arrayOfObjects) {
    // Check if arrayOfObjects is an array
    if (!Array.isArray(arrayOfObjects)) {
        throw new Error('Invalid input. Expected an array of objects.');
    }

    // Iterate over each object in arrayOfObjects and print age
    for (let i = 0; i < arrayOfObjects.length; i++) {
        const person = arrayOfObjects[i];
        const age = person.age || 'unknown'; // Assume age property exists in each object
        console.log(`Age of ${person.name || 'unknown'}: ${age}`);
    }
}

module.exports = printAges;
