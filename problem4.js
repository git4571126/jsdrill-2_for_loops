// problem4.js

function logIndividualAtIndex(arrayOfObjects, index) {
    // Check if arrayOfObjects is an array and index is within bounds
    if (!Array.isArray(arrayOfObjects) || index < 0 || index >= arrayOfObjects.length) {
        throw new Error('Invalid input. Expected an array of objects and a valid index.');
    }

    const individual = arrayOfObjects[index];
    const name = individual.name || 'unknown';
    const city = individual.city || 'unknown';

    console.log(`Name: ${name}, City: ${city}`);
}

module.exports = logIndividualAtIndex;
