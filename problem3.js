// problem3.js

function getStudentsInAustralia(arrayOfObjects) {
    // Check if arrayOfObjects is an array
    if (!Array.isArray(arrayOfObjects)) {
        throw new Error('Invalid input. Expected an array of objects.');
    }

    const studentsInAustralia = [];

    // Iterate over each object in arrayOfObjects
    for (let i = 0; i < arrayOfObjects.length; i++) {
        const person = arrayOfObjects[i];
        // Check if the person is a student and lives in Australia
        if (person.isStudent === true && person.country === 'Australia') {
            studentsInAustralia.push(person.name);
        }
    }

    // Check if any students in Australia were found
    if (studentsInAustralia.length === 0) {
        console.log('No students found who are living in Australia.');
    } else {
        // Display names of students in Australia
        console.log('Students living in Australia:');
        for (let i = 0; i < studentsInAustralia.length; i++) {
            console.log(studentsInAustralia[i]);
        }
    }
}

module.exports = getStudentsInAustralia;
