// problem6.js

function displayFirstHobby(arrayOfObjects) {
    // Check if arrayOfObjects is an array
    if (!Array.isArray(arrayOfObjects)) {
        throw new Error('Invalid input. Expected an array of objects.');
    }

    // Iterate over each object in arrayOfObjects and display first hobby
    for (let i = 0; i < arrayOfObjects.length; i++) {
        const person = arrayOfObjects[i];
        const firstHobby = person.hobbies && person.hobbies.length > 0 ? person.hobbies[0] : 'No hobbies';
        console.log(`First hobby of ${person.name || 'unknown'}: ${firstHobby}`);
    }
}

module.exports = displayFirstHobby;
