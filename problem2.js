// problem2.js

function getHobbiesByAge(arrayOfObjects, age) {
    // Check if arrayOfObjects is an array and age is a number
    if (!Array.isArray(arrayOfObjects) || typeof age !== 'number') {
        throw new Error('Invalid input. Expected an array of objects and a number.');
    }

    const matchedIndividuals = [];

    // Iterate over each object in arrayOfObjects to find matching individuals
    for (let i = 0; i < arrayOfObjects.length; i++) {
        const person = arrayOfObjects[i];
        if (person.age === age) {
            matchedIndividuals.push(person);
        }
    }

    // Check if any individuals matched the age
    if (matchedIndividuals.length === 0) {
        console.log(`No individuals found with age ${age}.`);
    } else {
        // Print hobbies of matched individuals
        for (let i = 0; i < matchedIndividuals.length; i++) {
            const person = matchedIndividuals[i];
            console.log(`Hobbies of ${person.name || 'unknown'} (${person.age} years old): ${person.hobbies.join(', ')}`);
        }
    }
}

module.exports = getHobbiesByAge;
