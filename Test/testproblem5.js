// test/testproblem5.js

const printAges = require('../problem5');
const arrayOfObjects = require('./dataset');

try {
    printAges(arrayOfObjects);
} catch (error) {
    console.error('Error:', error.message);
}
