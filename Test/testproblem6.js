// test/testproblem6.js

const displayFirstHobby = require('../problem6');
const arrayOfObjects = require('./dataset');

try {
    displayFirstHobby(arrayOfObjects);
} catch (error) {
    console.error('Error:', error.message);
}
