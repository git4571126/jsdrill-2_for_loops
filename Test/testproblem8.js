// test/testproblem8.js

const logCityAndCountry = require('../problem8');
const arrayOfObjects = require('./dataset');

try {
    logCityAndCountry(arrayOfObjects);
} catch (error) {
    console.error('Error:', error.message);
}
