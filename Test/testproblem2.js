
const getHobbiesByAge = require('../problem2');
const arrayOfObjects = require('./dataset');

try {
    getHobbiesByAge(arrayOfObjects, 30);
} catch (error) {
    console.error('Error:', error.message);
}
