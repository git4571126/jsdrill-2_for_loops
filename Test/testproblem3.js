
const getStudentsInAustralia = require('../problem3');
const arrayOfObjects = require('./dataset');

try {
    getStudentsInAustralia(arrayOfObjects);
} catch (error) {
    console.error('Error:', error.message);
}
