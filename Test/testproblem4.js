
const logIndividualAtIndex = require('../problem4');
const arrayOfObjects = require('./dataset');

try {
    logIndividualAtIndex(arrayOfObjects, 3);
} catch (error) {
    console.error('Error:', error.message);
}
