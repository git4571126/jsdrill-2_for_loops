const printNamesAndEmails = require('../problem7');
const arrayOfObjects = require('./dataset');

try {
    printNamesAndEmails(arrayOfObjects);
} catch (error) {
    console.error('Error:', error.message);
}
